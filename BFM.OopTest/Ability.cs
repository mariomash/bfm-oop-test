﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM.OopTest
{
    public enum AvailableAbility
    {
        Php, Html
    }

    public class Ability
    {
        public AvailableAbility Name { get; set; }
        public int Level { get; set; }
    }
}
