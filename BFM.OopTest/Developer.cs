﻿using System;

namespace BFM.OopTest
{
    class Developer : Employee, IFrontendCode, IBackendCode, IQualityAssurance
    {

        public Developer(ILogger logger, string name) : base(logger, name)
        {
        }

        //Interfaces Implementation

        public void DoFrontendCode(Project project)
        {
            Logger.Handle($"{Name} should write some frontend code now for the project {project.Name}");
        }

        public void DoBackendCode(Project project)
        {
            Logger.Handle($"{Name} should write some backend code now for the project {project.Name}");
        }

        public void DoSmokeTest(Project project)
        {
            Logger.Handle($"{this.Name} should test if code burns in project {project.Name}");
        }

        public void DoFunctionalTest(Project project)
        {
            Logger.Handle($"{this.Name} should test if code works in project {project.Name}");
        }

        public void DoPerformanceTest(Project project)
        {
            Logger.Handle($"{this.Name} should test if code melts in project {project.Name}");
        }

    }
}
