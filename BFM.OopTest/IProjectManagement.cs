﻿using System;

namespace BFM.OopTest
{
    internal interface IProjectManagement
    {
        void SetProjectStatus(Project project, ProjectStatuses status);

        void SetProjectOwner(Project project, object owner);

        void AddEmployeeToProject(Project project, Employee employee);
    }

}