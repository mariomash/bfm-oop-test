﻿using System;
using System.Collections.Generic;

namespace BFM.OopTest
{
    public enum ProjectStatuses
    {
        PreSales,
        Discovery,
        Planning,
        Design,
        Development,
        QualityAssurance,
        Launch
    }

    public abstract class Project
    {
        private object _owner;
        private bool _paid;
        private ProjectStatuses _status;


        protected Project(ILogger logger, string name, Client client)
        {
            Logger = logger;
            Name = name;
            Client = client;
            Owner = client;
            Status = ProjectStatuses.PreSales;
            Paid = false;
            Employees = new List<Employee>();
            RequiredAbilities = new List<AvailableAbility>();
        }

        private ILogger Logger { get; }

        public string Name { get; }
        public Client Client { get; }
        public List<AvailableAbility> RequiredAbilities { get; set; }
        public List<Employee> Employees { get; }

        public object Owner
        {
            get { return _owner; }
            private set
            {
                _owner = value;
                var ownerName = Owner.GetType().FullName;
                dynamic dOwner = Owner;
                if (dOwner.Name != null) ownerName = dOwner.Name.ToString();
                Logger.Handle($"{Name} new owner is: {ownerName}");
            }
        }

        public ProjectStatuses Status
        {
            get { return _status; }
            private set
            {
                _status = value;
                Logger.Handle($"{Name} new status is: {Enum.GetName(typeof (ProjectStatuses), _status)}");
            }
        }

        public bool Paid
        {
            get { return _paid; }
            private set
            {
                _paid = value;
                Logger.Handle($"{Name} new paid status is: {_paid}");
            }
        }

        public void SetPaid(Client client)
        {
            if (Client != client) return;
            Paid = true;
            if (Status == ProjectStatuses.PreSales) SetStatus(Client, ProjectStatuses.Discovery);
        }

        public void SetStatus(object owner, ProjectStatuses status)
        {
            if (Owner != owner) return;
            Status = status;
        }

        public void SetOwner(object owner, object newOwner)
        {
            if (Owner != owner) return;
            Owner = newOwner;
        }

        public void AddEmployee(object owner, Employee employee)
        {
            if (Owner != owner) return;

            if (!Employees.Contains(employee))
                Employees.Add(employee);
            if (!employee.Projects.Contains(this))
                employee.Projects.Add(this);
        }
    }
}