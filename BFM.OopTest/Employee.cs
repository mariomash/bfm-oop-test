﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM.OopTest
{
    public abstract class Employee
    {
        protected ILogger Logger { get; set; }
        public string Name { get; }
        public string Email { get; set; }
        public List<Project> Projects { get; set; }
        public List<Ability> Abilities { get; set; }

        protected Employee(ILogger logger, string name)
        {
            Logger = logger;
            Name = name;
            Projects = new List<Project>();
            Abilities = new List<Ability>();
        }
    }
}
