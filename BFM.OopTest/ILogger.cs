﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM.OopTest
{
    public interface ILogger
    {
        void Handle(string textToLog);
    }
}
