﻿namespace BFM.OopTest
{
    public class Client
    {
        public Client(ILogger logger, string name)
        {
            _logger = logger;
            Name = name;
        }

        private readonly ILogger _logger;

        public string Name { get; }
        public string Email { get; set; }

        public void PayProject(Project project)
        {
            _logger.Handle($"{Name} should pay now for the project: {project.Name}");
            project.SetPaid(this);
        }
    }
}