﻿namespace BFM.OopTest
{
    class ProjectManager : Employee, IProjectManagement, ILaunch
    {

        public ProjectManager(ILogger logger, string name) : base(logger, name)
        {
        }

        public void SetProjectStatus(Project project, ProjectStatuses status)
        {
            project.SetStatus(this, status);
        }

        public void SetProjectOwner(Project project, object owner)
        {
            Logger.Handle($"{Name} transferred ownership of {project.Name}");
            project.SetOwner(this, owner);
        }

        public void AddEmployeeToProject(Project project, Employee employee)
        {
            Logger.Handle($"{Name} added {employee.Name} to {project.Name}");
            project.AddEmployee(this, employee);
        }

        public void LaunchProject(Project project)
        {
            project.SetStatus(this, ProjectStatuses.Launch);
            Logger.Handle($"{project.Name} has been launched by {Name}!!!");
        }
    }
}
