﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BFM.OopTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var logger = new OutputLogger(textBox1);

            logger.Handle($"Let's start with a project{System.Environment.NewLine}--------------------------");

            var client = new Client(logger, @"Test Client") { Email = @"test_client@test.test" };

            var project = new WebSiteProject(logger, @"Test Project", client)
            {
                RequiredAbilities = new List<AvailableAbility>() { AvailableAbility.Html, AvailableAbility.Php }
            };

            var ad = new AccountDirector(logger, @"Test Account Director");

            client.PayProject(project);
            project.SetOwner(client, ad);
            ad.SetProjectStatus(project, ProjectStatuses.Planning);
            ad.SetProjectStatus(project, ProjectStatuses.Design);

            var pm = new ProjectManager(logger, @"Test Project Manager") { Email = @"test_pm@test.test" };

            ad.SetProjectOwner(project, pm);

            pm.SetProjectStatus(project, ProjectStatuses.Development);

            var dev = new Developer(logger, @"Test Developer") { Email = @"test_dev@test.test" };
            dev.Abilities.Add(new Ability() { Level = 5, Name = AvailableAbility.Html });
            dev.Abilities.Add(new Ability() { Level = 1, Name = AvailableAbility.Php });

            pm.AddEmployeeToProject(project, dev);

            dev.DoFrontendCode(project);
            dev.DoBackendCode(project);

            pm.SetProjectStatus(project, ProjectStatuses.QualityAssurance);

            dev.DoFunctionalTest(project);
            dev.DoSmokeTest(project);
            dev.DoPerformanceTest(project);

            pm.LaunchProject(project);
            pm.SetProjectOwner(project, ad);

            foreach (var employee in project.Employees)
            {
                foreach (var ability in employee.Abilities.Where(ability => project.RequiredAbilities.Contains(ability.Name)))
                {
                    logger.Handle($"Upgrading {ability.Name} skills for {employee.Name}");
                    ability.Level++;
                }
            }

            logger.Handle($"---------------------------{System.Environment.NewLine}Project Finished");

        }
    }
}
