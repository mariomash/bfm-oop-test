﻿namespace BFM.OopTest
{
    interface IQualityAssurance
    {
        void DoSmokeTest(Project project);
        void DoFunctionalTest(Project project);
        void DoPerformanceTest(Project project);
    }
}
