﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BFM.OopTest
{
    class AccountDirector : Employee, IProjectManagement
    {

        public AccountDirector(ILogger logger, string name) : base(logger, name)
        {
        }

        public void SetProjectStatus(Project project, ProjectStatuses status)
        {
            project.SetStatus(this, status);
        }

        public void SetProjectOwner(Project project, object owner)
        {
            Logger.Handle($"{Name} transferred ownership of {project.Name}");
            project.SetOwner(this, owner);
        }

        public void AddEmployeeToProject(Project project, Employee employee)
        {
            Logger.Handle($"{Name} added {employee.Name} to {project.Name}");
            project.AddEmployee(this, employee);
        }

    }
}
