﻿namespace BFM.OopTest
{
    internal interface ILaunch
    {
        void LaunchProject(Project project);
    }
}