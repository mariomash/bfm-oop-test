﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BFM.OopTest
{
    class OutputLogger : ILogger
    {
        private TextBox TextBox { get; }

        public OutputLogger(TextBox textBox)
        {
            TextBox = textBox;
        }

        public void Handle(string textToLog)
        {
            TextBox.AppendText($"{textToLog}{System.Environment.NewLine}");
            System.Diagnostics.Debug.WriteLine(textToLog);
        }
    }
}
